const fs = require("fs-extra");
const path = require("path");
const process = require("process");
const { succeed, fail, status } = require("../lib/spinner");

module.exports = async (appName, templateByLang) => {
  const templatePath = path.resolve(
    __dirname,
    "..",
    "templates",
    templateByLang
  );
  const currentDirectory = process.cwd();

  status("Downloading template...");

  try {
    await fs.copy(templatePath, path.resolve(currentDirectory, appName));
    succeed("Template downloaded");
  } catch (e) {
    fail(e.message);
  }
};
