const cp = require("child_process");
const path = require("path");
const { status, succeed, fail } = require("../lib/spinner");

const installModulesCallback = (resolve) => (err) => {
  if (err) {
    return fail(err.message);
  }

  return resolve();
};

module.exports = async (appName) => {
  await new Promise((resolve) => {
    const options = { cwd: path.resolve(appName) };

    cp.exec("npm i", options, installModulesCallback(resolve));
    status("Install node-modules...");
  });

  succeed("Modules installed successfully");
};
