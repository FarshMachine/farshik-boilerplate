const fs = require("fs-extra");
const path = require("path");
const h = require("../lib/highlights");
const { warn } = require("../lib/spinner");

module.exports = async (appName) => {
  const appDirectory = path.resolve(appName);
  const isDirectoryExists = await fs.pathExists(appDirectory);

  if (isDirectoryExists) {
    warn(
      h.warn(
        `App folder '${appName}' already exists, check the input parameters...`
      )
    );
    process.exit();
  }
};
