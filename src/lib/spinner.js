const ora = require("ora");
const { dots } = require("cli-spinners");

const spinner = ora({ spinner: dots });

const start = () => spinner.isSpinning || spinner.start();
const succeed = (msg) => spinner.succeed(`${msg}\r\n`);
const fail = (msg) => spinner.fail(`${msg}\r\n`);
const warn = (msg) => spinner.warn(`${msg}\r\n`);

const status = (msg) => {
  spinner.text = msg;
  start();
};

module.exports = {
  fail,
  start,
  status,
  succeed,
  warn,
};
