#!/usr/bin/env node

const { program } = require("commander");
const downloadTemplateByLang = require("./operations/download-template-by-lang");
const installModules = require("./operations/install-modules");
const checkExistingFolder = require("./operations/check-existing-folder");

program
  .option("-ts, --type-script", "React app with type-script")
  .parse(process.argv);

const init = async () => {
  const appName = program.args[0] || "react-boilerplate";
  const templateByLang = program.typeScript ? "react-app-ts" : "react-app-js";

  await checkExistingFolder(appName);
  await downloadTemplateByLang(appName, templateByLang);
  await installModules(appName);
};

init();
