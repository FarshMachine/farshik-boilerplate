const chalk = require('chalk')

exports.name = msg => chalk.magenta.bold(msg)
exports.warn = msg => chalk.yellow.bold(msg)
exports.err = msg => chalk.red.bold(msg)
exports.done = msg => chalk.green.bold(msg)
exports.right = exports.done