import { Switch, Route } from "react-router-dom";
import { hot } from "react-hot-loader/root";
import { ThemeLayout } from "@abdt/ornament";
import Main from "./scenes/Main";

const App = () => (
  <ThemeLayout>
    <div>Hello</div>
  </ThemeLayout>
);

export default hot(App);
